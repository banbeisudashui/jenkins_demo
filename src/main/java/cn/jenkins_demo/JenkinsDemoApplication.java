package cn.jenkins_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsDemoApplication {

    public static void main(String[] args) {
        //todo 123
        SpringApplication.run(JenkinsDemoApplication.class, args);
    }

}
